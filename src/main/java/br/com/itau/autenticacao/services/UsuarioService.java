package br.com.itau.autenticacao.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import br.com.itau.autenticacao.models.Usuario;
import br.com.itau.autenticacao.repositories.UsuarioRepository;

@Service
public class UsuarioService {
	
	@Autowired
	UsuarioRepository usuarioRepository;
	
	BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
	
	public Usuario inserir(Usuario usuario) {
		String hash = encoder.encode(usuario.getSenha());
		usuario.setSenha(hash);
		
		return usuarioRepository.save(usuario);
	}
	
	public Optional<Usuario> buscarPorId(int id) {
		return usuarioRepository.findById(id);
	}
	
	public Optional<Usuario> buscarPorEmailESenha(String nome, String senha) {
		Optional<Usuario> usuarioOptional = usuarioRepository.findByNome(nome);
		
		if(usuarioOptional.isPresent()) {
			Usuario usuario = usuarioOptional.get();
			
			if(encoder.matches(senha, usuario.getSenha())) {
				return usuarioOptional;
			}
		}
		
		return Optional.empty();
	}
}
