package br.com.itau.autenticacao.controllers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.autenticacao.helpers.JwtHelper;
import br.com.itau.autenticacao.models.Usuario;
import br.com.itau.autenticacao.services.UsuarioService;

@RestController
@RequestMapping("/login")
public class LoginController {

	@Autowired
	UsuarioService usuarioService;
	
	@PostMapping
	public ResponseEntity fazerLogin(@RequestBody Usuario usuario) {
		Optional<Usuario> usuarioOptional = usuarioService.buscarPorEmailESenha(
				usuario.getNome(), 
				usuario.getSenha());
		
		if(usuarioOptional.isPresent()) {
			Optional<String> tokenOptional = JwtHelper.gerar(usuarioOptional.get().getId());
			
			return ResponseEntity.ok(tokenOptional);
		}
		
		return ResponseEntity.status(400).build();
	}
}
